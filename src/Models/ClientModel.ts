export class ClientModel{
    id: number = 0;
    firstName: string = "";
    lastName: string = "";
    address:string = "";
    birthDate:Date = new Date();
    user_id:number = 0;
}