export class UserModel {
    id: number = 0;
    username: string = "";
    password: string = "";
    admin: boolean = false;
}