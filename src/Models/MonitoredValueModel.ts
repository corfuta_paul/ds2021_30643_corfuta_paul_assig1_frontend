export class MonitoredValueModel{
    id: number = 0;
    timeStamp: Date = new Date();
    energyConsumption: number = 0;
    sensor_id: number = 0;
}