export class DeviceModel{
    id: number = 0;
    description: string = "";
    address: string = "";
    maximumEnergyConsumption: number = 0;
    averageEnergyConsumption: number = 0;
    client_id: number = 0;
}