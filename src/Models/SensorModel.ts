export class SensorModel{
    id: number = 0;
    sensorDescription: string = "";
    maximumValue: number = 0;
    device_id: number = 0;
}