import axios from "axios";
import {ClientModel} from "../Models/ClientModel";
import {HOST} from "../Host";

const CLIENT_API_BASE_URL =`${HOST.backend_api}client/`;

class ClientService{
    getClients(): Promise<ClientModel[]> {
        return axios.get(CLIENT_API_BASE_URL).then((response)=>{
            return response.data;
        });
    }

    getClientById(id: number): Promise<ClientModel>{
        return axios.get(`${CLIENT_API_BASE_URL}${id}`).then((response)=>{
            return response.data;
        })
    }

    getClientByUserId(id: number): Promise<ClientModel>{
        return axios.get(`${CLIENT_API_BASE_URL}user/${id}`).then((response)=>{
            return response.data;
        })
    }

    updateClient(client: ClientModel):Promise<void> {
        return axios.put(CLIENT_API_BASE_URL,client).then((response)=>{
            return response.data;
        })
    }

    insertClient(client: ClientModel): Promise<number> {
        return axios.post(CLIENT_API_BASE_URL,client).then((response)=>{
            return response.data;
        })
    }

    deleteClient(id: number): Promise<void> {
        return axios.delete(`${CLIENT_API_BASE_URL}${id}`).then((response)=>{
            return response.data;
        })
    }
}

export default ClientService;