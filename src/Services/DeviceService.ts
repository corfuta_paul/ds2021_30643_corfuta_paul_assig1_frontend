import axios from "axios";
import {DeviceModel} from "../Models/DeviceModel";
import {HOST} from "../Host";

const DEVICE_API_BASE_URL =`${HOST.backend_api}device/`;

class DeviceService{
    getDevices():Promise<DeviceModel[]>{
        return axios.get(DEVICE_API_BASE_URL).then((response)=>{
            return response.data;
        })
    }

    getDevicesByClientId(id: number):Promise<DeviceModel[]> {
        return axios.get(`${DEVICE_API_BASE_URL}client/${id}`).then((response) => {
            return response.data;
        })
    }

    getDeviceById(id: number):Promise<DeviceModel>{
        return axios.get(`${DEVICE_API_BASE_URL}${id}`).then((response)=>{
            return response.data;
        })
    }

    insertDevice(device: DeviceModel):Promise<number>{
        return axios.post(DEVICE_API_BASE_URL,device).then((response)=>{
            return response.data;
        })
    }

    deleteDevice(id: number):Promise<void>{
        return axios.delete(`${DEVICE_API_BASE_URL}${id}`).then((response)=>{
            return response.data;
        })
    }

    updateDevice(device: DeviceModel):Promise<void>{
        return axios.put(DEVICE_API_BASE_URL,device).then((response)=>{
            return response.data;
        })
    }
}

export default DeviceService;