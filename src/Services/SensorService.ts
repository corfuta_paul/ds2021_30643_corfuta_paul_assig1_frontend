import axios from "axios";
import {SensorModel} from "../Models/SensorModel";
import {HOST} from "../Host";

const SENSOR_API_BASE_URL =`${HOST.backend_api}sensor/`;

class SensorService{
    getSensors():Promise<SensorModel[]>{
        return axios.get(SENSOR_API_BASE_URL).then((response)=>{
            return response.data;
        })
    }

    getSensorById(id: number):Promise<SensorModel>{
        return axios.get(`${SENSOR_API_BASE_URL}${id}`).then((response)=>{
            return response.data;
        })
    }

    getSensorByDeviceId(id: number):Promise<SensorModel>{
        return axios.get(`${SENSOR_API_BASE_URL}device/${id}`).then((response)=>{
            return response.data;
        })
    }

    insertSensor(sensor: SensorModel):Promise<number>{
        return axios.post(SENSOR_API_BASE_URL,sensor).then((response)=>{
            return response.data;
        })
    }

    deleteSensor(id: number):Promise<void>{
        return axios.delete(`${SENSOR_API_BASE_URL}${id}`).then((response)=>{
            return response.data;
        })
    }

    updateSensor(sensor: SensorModel):Promise<void>{
        return axios.put(SENSOR_API_BASE_URL,sensor).then((response)=>{
            return response.data;
        })
    }
}

export default SensorService;