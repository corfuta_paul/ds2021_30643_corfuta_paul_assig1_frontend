import axios from "axios";
import {UserModel} from "../Models/UserModel";
import {HOST} from "../Host";

const USER_API_BASE_URL =`${HOST.backend_api}user/`;
const USER_API_BASE_URL_LOGIN =`${HOST.backend_api}user/login/`;

class UserService {
    getUsers(): Promise<UserModel[]> {
        return axios.get(USER_API_BASE_URL).then((response)=>{
            return response.data;
        })
    }
    getUserById(id: number): Promise<UserModel>{
        return axios.get(`${USER_API_BASE_URL}${id}`).then((response)=>{
            return response.data;
        })
    }

    updateUser(user: UserModel): Promise<void>{
        return axios.put(USER_API_BASE_URL,user).then((response)=>{
            return response.data;
        })
    }

    insertUser(user: UserModel): Promise<number> {
        return axios.post(USER_API_BASE_URL,user).then((response)=>{
            return response.data;
        })
    }
    deleteUser(id: number): Promise<void> {
        return axios.delete(`${USER_API_BASE_URL}${id}`).then((response)=>{
            return response.data;
        })
    }

    login(user: UserModel): Promise<UserModel> {
        return axios.post(USER_API_BASE_URL_LOGIN,user).then((response)=>{
            return response.data;
        })
    }

}

export default UserService;