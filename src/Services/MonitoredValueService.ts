import axios from "axios";
import {MonitoredValueModel} from "../Models/MonitoredValueModel";
import {HOST} from "../Host";

const MONITOREDVALUE_API_BASE_URL =`${HOST.backend_api}monitoredvalue/`;

class MonitoredValueService{
    getMonitoredValues():Promise<MonitoredValueModel[]>{
        return axios.get(MONITOREDVALUE_API_BASE_URL).then((response)=>{
            return response.data;
        })
    }

    getMonitoredValueById(id: number):Promise<MonitoredValueModel>{
        return axios.get(`${MONITOREDVALUE_API_BASE_URL}${id}`).then((response)=>{
            return response.data;
        })
    }

    getMonitoredValuesBySensorId(id: number):Promise<MonitoredValueModel[]>{
        return axios.get(`${MONITOREDVALUE_API_BASE_URL}sensor/${id}`).then((response)=>{
            return response.data;
        })
    }

    insertMonitoredValue(monitoredValue: MonitoredValueModel):Promise<number>{
        return axios.post(MONITOREDVALUE_API_BASE_URL,monitoredValue).then((response)=>{
            return response.data;
        })
    }

    deleteMonitoredValue(id: number):Promise<void>{
        return axios.delete(`${MONITOREDVALUE_API_BASE_URL}${id}`).then((response)=>{
            return response.data;
        })
    }

    updateMonitoredValue(monitoredValue: MonitoredValueModel):Promise<void>{
        return axios.put(MONITOREDVALUE_API_BASE_URL,monitoredValue).then((response)=> {
            return response.data;
        })
    }
}

export default MonitoredValueService;