import React from "react";
import {Button, Table} from "semantic-ui-react";
import {SensorModel} from "../../Models/SensorModel";
import {MonitoredValueModel} from "../../Models/MonitoredValueModel";

interface IMonitoredValueList {
    sensors: SensorModel[];
    monitoredValues: MonitoredValueModel[];
    isLoading: boolean;
    onCreate: Function;
    onEdit: Function;
    onDelete: Function;
}

const MonitoredValueList: React.FunctionComponent<IMonitoredValueList> = (props) => {

    const onDeleteHandler = (id: number) => {
        props.onDelete(id);
    }

    const onCreateHandler = () => {
        props.onCreate();
    }

    const onEditHandler = (id: number) => {
        props.onEdit(id);
    }

    return <>
        {!props.isLoading && <Table striped>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell width={2}><Button color={"teal"} onClick={onCreateHandler}>Create</Button></Table.HeaderCell>
                    <Table.HeaderCell>Time Stamp</Table.HeaderCell>
                    <Table.HeaderCell>Energy Consumption</Table.HeaderCell>
                    <Table.HeaderCell>Sensor</Table.HeaderCell>
                </Table.Row>
            </Table.Header>

            <Table.Body>
                {props.monitoredValues.map(monitoredValue => (
                    <Table.Row key={monitoredValue.id}>
                        <Table.Cell>
                            <Button icon="pencil" color="teal" onClick={() => {
                                onEditHandler(monitoredValue.id)
                            }}/>
                            <Button icon="trash" color="red" onClick={() => {
                                onDeleteHandler(monitoredValue.id)
                            }}/>
                        </Table.Cell>
                        <Table.Cell>{`${new Date(monitoredValue.timeStamp).toLocaleDateString("ro-RO")} - ${new Date(monitoredValue.timeStamp).toLocaleTimeString("ro-RO")}`}</Table.Cell>
                        <Table.Cell>{monitoredValue.energyConsumption}</Table.Cell>
                        <Table.Cell>{props.sensors.find(q => q.id === monitoredValue.sensor_id)?.sensorDescription}</Table.Cell>
                    </Table.Row>
                ))}
            </Table.Body>
        </Table>}
    </>
}

export default MonitoredValueList;