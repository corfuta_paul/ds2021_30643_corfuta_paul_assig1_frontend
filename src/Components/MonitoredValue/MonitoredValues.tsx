import {Loader} from "semantic-ui-react";
import React, {useEffect, useState} from "react";
import SensorService from "../../Services/SensorService";
import {SensorModel} from "../../Models/SensorModel";
import MonitoredValueService from "../../Services/MonitoredValueService";
import {MonitoredValueModel} from "../../Models/MonitoredValueModel";
import MonitoredValueList from "./MonitoredValueList";
import MonitoredValueForm from "./MonitoredValueForm";
import AppMenu from "../AppMenu";
import {routes} from "../../App";



const sensorService: SensorService = new SensorService();
const monitoredValueService: MonitoredValueService = new MonitoredValueService();

const Sensors: React.FunctionComponent<{}> = () => {
    const [sensors, setSensors] = useState<SensorModel[]>([]);
    const [monitoredValues, setMonitoredValues] = useState<MonitoredValueModel[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [error, setError] = useState<string>("");
    const [isFormOpen, setIsFormOpen] = useState<boolean>(false);
    const [currentMonitoredValue, setCurrentMonitoredValue] = useState<MonitoredValueModel>({
        id: 0,
        timeStamp:new Date(),
        energyConsumption:0,
        sensor_id:0
    });
    const [currentSensor, setCurrentSensor] = useState<SensorModel>({
        id: 0,
        sensorDescription:"",
        maximumValue:0,
        device_id:0
    });

    useEffect(() => {
        let isMounted = true;
        setIsLoading(true);

        let fn = async () => {
            try {
                let sensors = await sensorService.getSensors();
                let monitoredValues = await monitoredValueService.getMonitoredValues();
                if (isMounted) {
                    setSensors(sensors);
                    setMonitoredValues(monitoredValues);
                    setIsLoading(false);
                }
            } catch (error) {
                console.log(error);
                if (isMounted) {
                    setError("Something went wrong while getting users/clients");
                }
            }
        }

        fn();

        return () => {
            isMounted = false;
        }
    }, [])

    const onDeleteHandler = (id: number) => {
        monitoredValueService.deleteMonitoredValue(id).then(() => {
            setMonitoredValues(monitoredValues.filter(q=>q.id!==id));
        });

    }

    const onCreateHandler = () => {
        setCurrentSensor({
            id: 0,
            sensorDescription:"",
            maximumValue:0,
            device_id:0});
        setCurrentMonitoredValue({
            id: 0,
            timeStamp:new Date(),
            energyConsumption:0,
            sensor_id:0})
        setIsFormOpen(true);
    }

    const onSubmitHandler = (monitoredValue: MonitoredValueModel) => {
        if (monitoredValue.id === 0) {
            monitoredValueService.insertMonitoredValue(monitoredValue).then((response) => {
                monitoredValue.id = response;
                setMonitoredValues([...monitoredValues, monitoredValue]);
                setIsFormOpen(false);
            });
        }
        else {
            monitoredValueService.updateMonitoredValue(monitoredValue).then(() => {
                let index: number = monitoredValues.findIndex(q => q.id === monitoredValue.id);
                monitoredValues[index] = monitoredValue;
                setMonitoredValues(monitoredValues);
                setIsFormOpen(false);
            })
        }

    }

    const onCancelHandler = () => {
        setIsFormOpen(false);
    }

    const onEditHandler = (id: number) => {
        monitoredValueService.getMonitoredValueById(id).then((response) => {
            let monitoredValue1=response;
            sensorService.getSensorById(response.sensor_id).then((response)=>{
                setCurrentSensor(response);
                monitoredValue1.sensor_id=response.id;
                setCurrentMonitoredValue(monitoredValue1);
                setIsFormOpen(true);
            })

        })

    }

    return <>
        <Loader active={isLoading} size={"big"}>Loading</Loader>
        <MonitoredValueList sensors={sensors} isLoading={isLoading} onDelete={onDeleteHandler} onCreate={onCreateHandler} monitoredValues={monitoredValues}
                    onEdit={onEditHandler}/>
        <MonitoredValueForm isFormOpen={isFormOpen} onCancel={onCancelHandler} onSubmit={onSubmitHandler}
                    currentSensor={currentSensor!} currentMonitoredValue={currentMonitoredValue!} sensors={sensors}/>
    </>

}

export default Sensors;