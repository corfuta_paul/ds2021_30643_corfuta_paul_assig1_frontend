import React, {useEffect, useState} from "react";
import {Button, Form, Modal, Container, Select} from "semantic-ui-react";
import {SensorModel} from "../../Models/SensorModel";
import {MonitoredValueModel} from "../../Models/MonitoredValueModel";
import {Alert, Stack} from "@mui/material";
import BlueButton from "../Styles/BlueButton";
import PurpleButton from "../Styles/PurpleButton";



interface IMonitoredValueForm {
    isFormOpen: boolean;
    onCancel: Function;
    onSubmit: Function;
    currentSensor: SensorModel;
    currentMonitoredValue : MonitoredValueModel;
    sensors: SensorModel[];
}

interface IDropdownItem{
    key:number,
    value:number,
    text:string;
}

const MonitoredValueForm: React.FunctionComponent<IMonitoredValueForm> = (props) => {

    const [sensor, setSensor] = useState<SensorModel>(props.currentSensor);
    const [monitoredValue, setMonitoredValue] = useState<MonitoredValueModel>(props.currentMonitoredValue);
    const [error, setError] = useState<string>("");

    useEffect(()=>{
        setSensor(props.currentSensor);
        setMonitoredValue(props.currentMonitoredValue);
    },[props.currentSensor,props.currentMonitoredValue])


    const onCancelHandler = () => {
        props.onCancel();
    }

    const onSubmitHandler = (mv: MonitoredValueModel) => {
        if(mv.energyConsumption<0){
            setError("Energy consumption must be greater than 0");
            return;
        }
        if(mv.sensor_id===0){
            setError("Every monitored value must bu associated to a sensor");
            return;
        }
        props.onSubmit(mv);
    }

    const onCloseModal = () => {
        setError("");
    }

    const mapToDropdownList = (s:SensorModel):IDropdownItem =>{
        return {
            key:s.id,
            value:s.id,
            text:s.sensorDescription
        }
    }

    const onChangeEnergyHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setMonitoredValue(prevState => ({
            ...prevState,
            energyConsumption:event.target.valueAsNumber
        }))
    }


    const onChangeSensor = (event: React.ChangeEvent<HTMLSelectElement>, data:any) => {
        setSensor(prevState => ({
            ...prevState,
            id: data.value
        }))
        setMonitoredValue(prevState => ({
            ...prevState,
            sensor_id: data.value
        }))
    }

    return <>
        <Modal
            size={"mini"}
            open={error!==""}
            onClose={onCloseModal}
        >
            <Alert severity="error">{error}</Alert>
        </Modal>
        <Modal
        open={props.isFormOpen}
        onClose={onCancelHandler}
        size={"mini"}
        dimmer={"blurring"}
    >
        <Modal.Header>
            <Container fluid textAlign="center">{monitoredValue.id===0 ? "Create Monitored Value" : "Edit Monitored Value"}</Container>
        </Modal.Header>
        <Modal.Content>
            <Form>
                <Form.Field required>
                    <label>Energy Consumption</label>
                    <input placeholder='Energy Consumption' value={monitoredValue.energyConsumption} type={"number"} min={0} onChange={onChangeEnergyHandler}/>
                </Form.Field>
                {monitoredValue.id===0 &&
                <Form.Field
                    search
                    required
                    value={sensor.id}
                    onChange={onChangeSensor}
                    label={"Sensor"}
                    control={Select}
                    options={props.sensors.map(sensor => mapToDropdownList(sensor))}
                />}
                {monitoredValue.id!==0 &&
                <Form.Field>
                    <label>Sensor</label>
                    <input placeholder='Sensor' value={sensor.sensorDescription} readOnly={true}/>
                </Form.Field>
                }
                <Form.Field>
                    <Stack direction="row"
                           justifyContent="center"
                           alignItems="center"
                           spacing={2}>
                        <BlueButton onClick={() => {
                            onSubmitHandler(monitoredValue)
                        }} >{
                            monitoredValue.id===0 ? "Create" : "Edit"}</BlueButton>
                        <PurpleButton onClick={onCancelHandler} type={"button"}>Close</PurpleButton>
                    </Stack>
                </Form.Field>
            </Form>
        </Modal.Content>

    </Modal>
        </>
}

export default MonitoredValueForm;