import React from "react";
import {Button, Table} from "semantic-ui-react";
import {ClientModel} from "../../Models/ClientModel";
import {UserModel} from "../../Models/UserModel";

interface IClientList {
    clients: ClientModel[];
    users: UserModel[];
    isLoading: boolean;
    onCreate:Function;
    onEdit:Function;
    onDelete: Function;
}

const ClientList:React.FunctionComponent<IClientList> = (props) =>{

    const onDeleteHandler = (id: number) =>{
        props.onDelete(id);
    }

    const onCreateHandler = () =>{
        props.onCreate();
    }

    const onEditHandler = (id: number) => {
        props.onEdit(id);
    }

    return <>
        { !props.isLoading && <Table striped >
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell width={2}><Button color={"teal"} onClick={onCreateHandler}>Create</Button></Table.HeaderCell>
                    <Table.HeaderCell>Name</Table.HeaderCell>
                    <Table.HeaderCell width={5}>Address</Table.HeaderCell>
                    <Table.HeaderCell>Birth Date</Table.HeaderCell>
                    <Table.HeaderCell>Username</Table.HeaderCell>
                </Table.Row>
            </Table.Header>

            <Table.Body>
                {props.clients.map(client =>(
                    <Table.Row key={client.id}>
                        <Table.Cell>
                            <Button icon="pencil" color="teal" onClick={()=>{onEditHandler(client.id)}}/>
                            <Button icon="trash" color="red" onClick={()=>{onDeleteHandler(client.id)}}/>
                        </Table.Cell>
                        <Table.Cell>{`${client.firstName} ${client.lastName}`}</Table.Cell>
                        <Table.Cell>{client.address}</Table.Cell>
                        <Table.Cell>{`${new Date(client.birthDate).toLocaleDateString("ro-RO")}`}</Table.Cell>
                        <Table.Cell>{props.users.find(q=>q.id===client.user_id)?.username}</Table.Cell>
                    </Table.Row>
                ))}
            </Table.Body>
        </Table>}
    </>
}

export default ClientList;