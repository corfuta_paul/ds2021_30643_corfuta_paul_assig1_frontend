import {Loader} from "semantic-ui-react";
import React, {useEffect, useState} from "react";
import ClientService from "../../Services/ClientService";
import {ClientModel} from "../../Models/ClientModel";
import ClientList from "./ClientList";
import ClientForm from "./ClientForm";
import UserService from "../../Services/UserService";
import {UserModel} from "../../Models/UserModel";
import AppMenu from "../AppMenu";
import {routes} from "../../App";

const clientService: ClientService = new ClientService();
const userService: UserService = new UserService();

const Clients: React.FunctionComponent<{}> = () => {
    const [clients, setClients] = useState<ClientModel[]>([]);
    const [users, setUsers] = useState<UserModel[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [error, setError] = useState<string>("");
    const [isFormOpen, setIsFormOpen] = useState<boolean>(false);
    const [currentClient, setCurrentClient] = useState<ClientModel>({
        id: 0,
        firstName: "",
        lastName: "",
        address: "",
        birthDate: new Date(),
        user_id: 0
    });
    const [currentUser, setCurrentUser] = useState<UserModel>({id: 0, username: "", password: "", admin: false});

    useEffect(() => {
        let isMounted = true;
        setIsLoading(true);

        let fn = async () => {
            try {
                let clients = await clientService.getClients();
                let users = await userService.getUsers();
                if (isMounted) {
                    setClients(clients);
                    setUsers(users);
                    setIsLoading(false);
                }
            } catch (error) {
                console.log(error);
                if (isMounted) {
                    setError("Something went wrong while getting users/clients");
                }
            }
        }

        fn();

        return () => {
            isMounted = false;
        }
    }, [])

    const onDeleteHandler = (id: number) => {
        clientService.deleteClient(id).then(() => {
            setClients(clients.filter(q => q.id !== id));
        });

    }

    const onCreateHandler = () => {
        setCurrentClient({id: 0, firstName: "", lastName: "", address: "", birthDate: new Date(), user_id: 0});
        setCurrentUser({id: 0, username: "", password: "", admin: false})
        setIsFormOpen(true);
    }

    const onSubmitHandler = (client: ClientModel) => {
        if (client.id === 0) {
            clientService.insertClient(client).then((response) => {
                client.id = response;
                setClients([...clients, client]);
                setIsFormOpen(false);
            });
            } else {
                clientService.updateClient(client).then(() => {
                    let index: number = clients.findIndex(q => q.id === client.id);
                    clients[index] = client;
                    setClients(clients);
                    setIsFormOpen(false);
                })
            }

    }

        const onCancelHandler = () => {
            setIsFormOpen(false);
        }

        const onEditHandler = (id: number) => {
            clientService.getClientById(id).then((response) => {
                let client1=response;
                userService.getUserById(response.user_id).then((response)=>{
                    setCurrentUser(response);
                    client1.user_id=response.id;
                    setCurrentClient(client1);
                    setIsFormOpen(true);
                })

            })

        }

        return <>
            <Loader active={isLoading} size={"big"}>Loading</Loader>
            <ClientList clients={clients} isLoading={isLoading} onDelete={onDeleteHandler} onCreate={onCreateHandler} users={users}
                        onEdit={onEditHandler}/>
            <ClientForm isFormOpen={isFormOpen} onCancel={onCancelHandler} onSubmit={onSubmitHandler}
                      currentClient={currentClient!} currentUser={currentUser!} users={users}/>
        </>

}

export default Clients;