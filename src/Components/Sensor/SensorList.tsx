import React from "react";
import {Button, Table} from "semantic-ui-react";
import {DeviceModel} from "../../Models/DeviceModel";
import {SensorModel} from "../../Models/SensorModel";

interface ISensorList {
    sensors: SensorModel[];
    devices: DeviceModel[];
    isLoading: boolean;
    onCreate:Function;
    onEdit:Function;
    onDelete: Function;
}

const SensorList:React.FunctionComponent<ISensorList> = (props) =>{

    const onDeleteHandler = (id: number) =>{
        props.onDelete(id);
    }

    const onCreateHandler = () =>{
        props.onCreate();
    }

    const onEditHandler = (id: number) => {
        props.onEdit(id);
    }

    return <>
        { !props.isLoading && <Table striped >
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell width={2}><Button color={"teal"} onClick={onCreateHandler}>Create</Button></Table.HeaderCell>
                    <Table.HeaderCell>Description</Table.HeaderCell>
                    <Table.HeaderCell>Maximum Value</Table.HeaderCell>
                    <Table.HeaderCell>Device</Table.HeaderCell>
                </Table.Row>
            </Table.Header>

            <Table.Body>
                {props.sensors.map(sensor =>(
                    <Table.Row key={sensor.id}>
                        <Table.Cell>
                            <Button icon="pencil" color="teal" onClick={()=>{onEditHandler(sensor.id)}}/>
                            <Button icon="trash" color="red" onClick={()=>{onDeleteHandler(sensor.id)}}/>
                        </Table.Cell>
                        <Table.Cell>{sensor.sensorDescription}</Table.Cell>
                        <Table.Cell>{sensor.maximumValue}</Table.Cell>
                        <Table.Cell>{props.devices.find(q=>q.id===sensor.device_id)?.description}</Table.Cell>

                    </Table.Row>
                ))}
            </Table.Body>
        </Table>}
    </>
}

export default SensorList;