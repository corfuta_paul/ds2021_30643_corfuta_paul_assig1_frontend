import React, {useEffect, useState} from "react";
import {Button, Form, Modal, Container, Select} from "semantic-ui-react";
import {DeviceModel} from "../../Models/DeviceModel";
import {SensorModel} from "../../Models/SensorModel";
import {Alert, Stack} from "@mui/material";
import BlueButton from "../Styles/BlueButton";
import PurpleButton from "../Styles/PurpleButton";



interface ISensorForm {
    isFormOpen: boolean;
    onCancel: Function;
    onSubmit: Function;
    currentSensor: SensorModel;
    currentDevice: DeviceModel;
    devices: DeviceModel[];
}

interface IDropdownItem{
    key:number,
    value:number,
    text:string;
}

const SensorForm: React.FunctionComponent<ISensorForm> = (props) => {

    const [sensor, setSensor] = useState<SensorModel>(props.currentSensor);
    const [device, setDevice] = useState<DeviceModel>(props.currentDevice);
    const [error,setError] = useState<string>("");

    useEffect(()=>{
        setSensor(props.currentSensor);
        setDevice(props.currentDevice);
    },[props.currentSensor,props.currentDevice])

    const onCancelHandler = () => {
        props.onCancel();
    }

    const onSubmitHandler = (s: SensorModel) => {
        if(s.sensorDescription.trim() === ""){
            setError("Invalid description.")
            return;
        }
        if(s.maximumValue <0){
            setError("Invalid maximum value.")
            return;
        }
        if(s.device_id === 0){
            setError("The sensor must be associated to a device.")
            return;
        }
        props.onSubmit(s);
    }

    const onCloseModal = () => {
        setError("");
    }

    const mapToDropdownList = (dv:DeviceModel):IDropdownItem =>{
        return {
            key:dv.id,
            value:dv.id,
            text:dv.description
        }
    }

    const onChangeDescriptionHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSensor(prevState => ({
            ...prevState,
            sensorDescription:event.target.value
        }))
    }


    const onChangeMaximumHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSensor(prevState => ({
            ...prevState,
           maximumValue:event.target.valueAsNumber
        }))
    }


    const onChangeClient = (event: React.ChangeEvent<HTMLSelectElement>, data:any) => {
        setSensor(prevState => ({
            ...prevState,
            device_id: data.value
        }))
        setDevice(prevState => ({
            ...prevState,
            id: data.value
        }))
    }

    return <>
        <Modal
            size={"mini"}
            open={error!==""}
            onClose={onCloseModal}
        >
            <Alert severity="error">{error}</Alert>
        </Modal>
        <Modal
        open={props.isFormOpen}
        onClose={onCancelHandler}
        size={"mini"}
        dimmer={"blurring"}
    >
        <Modal.Header>
            <Container fluid textAlign="center">{sensor.id===0 ? "Create Sensor" : "Edit Sensor"}</Container>
        </Modal.Header>
        <Modal.Content>
            <Form>
                <Form.Field required>
                    <label>Description</label>
                    <input placeholder='Description' value={sensor.sensorDescription} onChange={onChangeDescriptionHandler}/>
                </Form.Field>
                <Form.Field required>
                    <label>Maximum value</label>
                    <input placeholder='Maximum value' value={sensor.maximumValue} type={"number"} min={0} onChange={onChangeMaximumHandler}/>
                </Form.Field>
                {sensor.id===0 &&
                <Form.Field
                    search
                    required
                    value={device.id}
                    onChange={onChangeClient}
                    label={"Device"}
                    control={Select}
                    options={props.devices.map(device => mapToDropdownList(device))}
                />}
                {sensor.id!==0 &&
                <Form.Field>
                    <label>Device</label>
                    <input placeholder='Device' value={device.description} readOnly={true}/>
                </Form.Field>
                }
                <Form.Field>
                    <Stack direction="row"
                           justifyContent="center"
                           alignItems="center"
                           spacing={2}>
                        <BlueButton onClick={() => {
                            onSubmitHandler(sensor)
                        }} >{
                            sensor.id===0 ? "Create" : "Edit"}</BlueButton>
                        <PurpleButton onClick={onCancelHandler} type={"button"}>Close</PurpleButton>
                    </Stack>
                </Form.Field>
            </Form>
        </Modal.Content>

    </Modal>
        </>
}

export default SensorForm;