import {Loader} from "semantic-ui-react";
import React, {useEffect, useState} from "react";
import DeviceService from "../../Services/DeviceService";
import {DeviceModel} from "../../Models/DeviceModel";
import SensorService from "../../Services/SensorService";
import {SensorModel} from "../../Models/SensorModel";
import SensorList from "./SensorList";
import SensorForm from "./SensorForm";
import AppMenu from "../AppMenu";
import {routes} from "../../App";


const sensorService: SensorService = new SensorService();
const deviceService: DeviceService = new DeviceService();

const Sensors: React.FunctionComponent<{}> = () => {
    const [sensors, setSensors] = useState<SensorModel[]>([]);
    const [devices, setDevices] = useState<DeviceModel[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [error, setError] = useState<string>("");
    const [isFormOpen, setIsFormOpen] = useState<boolean>(false);
    const [currentDevice, setCurrentDevice] = useState<DeviceModel>({
        id: 0,
        description: "",
        address:"",
        averageEnergyConsumption:0,
        maximumEnergyConsumption:0,
        client_id:0
    });
    const [currentSensor, setCurrentSensor] = useState<SensorModel>({
        id: 0,
        sensorDescription:"",
        maximumValue:0,
        device_id:0
    });

    useEffect(() => {
        let isMounted = true;
        setIsLoading(true);

        let fn = async () => {
            try {
                let sensors = await sensorService.getSensors();
                let devices = await deviceService.getDevices();
                if (isMounted) {
                    setSensors(sensors);
                    setDevices(devices);
                    setIsLoading(false);
                }
            } catch (error) {
                console.log(error);
                if (isMounted) {
                    setError("Something went wrong while getting users/clients");
                }
            }
        }

        fn();

        return () => {
            isMounted = false;
        }
    }, [])

    const onDeleteHandler = (id: number) => {
        sensorService.deleteSensor(id).then(() => {
            setSensors(sensors.filter(q=>q.id!==id));
        });

    }

    const onCreateHandler = () => {
        setCurrentSensor({id: 0,
            sensorDescription:"",
            maximumValue:0,
            device_id:0});
        setCurrentDevice({  id: 0,
            description: "",
            address:"",
            averageEnergyConsumption:0,
            maximumEnergyConsumption:0,
            client_id:0})
        setIsFormOpen(true);
    }

    const onSubmitHandler = (sensor: SensorModel) => {
        if (sensor.id === 0) {
            sensorService.insertSensor(sensor).then((response) => {
                sensor.id = response;
                setSensors([...sensors, sensor]);
                setIsFormOpen(false);
            });
        }
        else {
            sensorService.updateSensor(sensor).then(() => {
                let index: number = sensors.findIndex(q => q.id === sensor.id);
                sensors[index] = sensor;
                setSensors(sensors);
                setIsFormOpen(false);
            })
        }

    }

    const onCancelHandler = () => {
        setIsFormOpen(false);
    }

    const onEditHandler = (id: number) => {
        sensorService.getSensorById(id).then((response) => {
            let sensor1=response;
            deviceService.getDeviceById(response.device_id).then((response)=>{
                setCurrentDevice(response);
                sensor1.device_id=response.id;
                setCurrentSensor(sensor1);
                setIsFormOpen(true);
            })

        })

    }

    return <>
        <Loader active={isLoading} size={"big"}>Loading</Loader>
        <SensorList sensors={sensors} isLoading={isLoading} onDelete={onDeleteHandler} onCreate={onCreateHandler} devices={devices}
                    onEdit={onEditHandler}/>
        <SensorForm isFormOpen={isFormOpen} onCancel={onCancelHandler} onSubmit={onSubmitHandler}
                    currentSensor={currentSensor!} currentDevice={currentDevice!} devices={devices}/>
    </>

}

export default Sensors;