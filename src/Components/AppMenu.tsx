import {Dropdown, DropdownProps, Menu, MenuItem} from 'semantic-ui-react';
import React, {SyntheticEvent, useEffect, useState} from "react";
import {useHistory} from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css'
import {UserModel} from "../Models/UserModel";
import {useGlobalContext} from "../AuthContext";


interface IMenuObject {
    name: string;
    path: string;
}

interface MenuList {
    key: string;
    value: string;
    text: string;
}

const AppMenu: React.FunctionComponent<{routes: IMenuObject[]}> = (props) =>{

    const {isLoggedIn, setIsLoggedIn} = useGlobalContext();
    const [isAdmin,setIsAdmin]=useState<boolean>(false);
    const history = useHistory();

    useEffect(()=>{
        console.log(isLoggedIn);
        const user:UserModel=JSON.parse(sessionStorage.getItem("User")!);

        if(user?.admin===true){
            setIsAdmin(true);
        }
        else{
            setIsAdmin(false);
        }

    },[isLoggedIn])

    const redirect = (route:string) => {
        history.push(route);
    }

    const MapToMenuList:MenuList[] = props.routes.map(menuItem => {
        return{
            key: menuItem.name,
            value: menuItem.path,
            text: menuItem.name
        }
    })

    const pageChangeHandler = (event:SyntheticEvent<HTMLElement,Event>, data:DropdownProps) => {
        const value:string = data.value!.toString();
        redirect(value);
    }

    const onLogoutHandler =  () => {
        sessionStorage.removeItem('User');
        setIsLoggedIn(false);
        redirect("/login")
    }

    return <Menu color="teal" inverted size={"massive"}>
        <Menu.Item header onClick={() => redirect("/home")} name="Home"/>

        {isLoggedIn && isAdmin && <Menu.Item>
                <Dropdown
                        clearable
                        text="Manage database"
                        onChange={pageChangeHandler}
                        options={MapToMenuList}
                />
        </Menu.Item>}
        {!isLoggedIn && <MenuItem onClick={() => redirect("/register")} position="right" icon="user plus"/>}
        {!isLoggedIn && <MenuItem onClick={() => redirect("/login")}  icon="sign-in"/>}
        {isLoggedIn && <MenuItem onClick={() => redirect("/profile") }  name={"Profile"}/>}
        {isLoggedIn && <MenuItem onClick={() => onLogoutHandler() } position="right"  icon="sign-out"/>}
    </Menu>
}

export default AppMenu