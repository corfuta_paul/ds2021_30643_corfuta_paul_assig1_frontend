import {Loader} from "semantic-ui-react";
import React, {useEffect, useState} from "react";
import ClientService from "../../Services/ClientService";
import {ClientModel} from "../../Models/ClientModel";
import DeviceService from "../../Services/DeviceService";
import {DeviceModel} from "../../Models/DeviceModel";
import DeviceList from "./DeviceList";
import DeviceForm from "./DeviceForm";
import AppMenu from "../AppMenu";
import {routes} from "../../App";

const clientService: ClientService = new ClientService();
const deviceService: DeviceService = new DeviceService();

const Clients: React.FunctionComponent<{}> = () => {
    const [clients, setClients] = useState<ClientModel[]>([]);
    const [devices, setDevices] = useState<DeviceModel[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [error, setError] = useState<string>("");
    const [isFormOpen, setIsFormOpen] = useState<boolean>(false);
    const [currentDevice, setCurrentDevice] = useState<DeviceModel>({
        id: 0,
        description: "",
        address:"",
        averageEnergyConsumption:0,
        maximumEnergyConsumption:0,
        client_id:0
    });
    const [currentClient, setCurrentClient] = useState<ClientModel>({
        id: 0,
        firstName: "",
        lastName: "",
        address: "",
        birthDate: new Date(),
        user_id: 0
    });

    useEffect(() => {
        let isMounted = true;
        setIsLoading(true);

        let fn = async () => {
            try {
                let clients = await clientService.getClients();
                let devices = await deviceService.getDevices();
                if (isMounted) {
                    setClients(clients);
                    setDevices(devices);
                    setIsLoading(false);
                }
            } catch (error) {
                console.log(error);
                if (isMounted) {
                    setError("Something went wrong while getting users/clients");
                }
            }
        }

        fn();

        return () => {
            isMounted = false;
        }
    }, [])

    const onDeleteHandler = (id: number) => {
        deviceService.deleteDevice(id).then(() => {
            setDevices(devices.filter(q=>q.id!==id));
        });

    }

    const onCreateHandler = () => {
        setCurrentClient({id: 0, firstName: "", lastName: "", address: "", birthDate: new Date(), user_id: 0});
        setCurrentDevice({  id: 0,
            description: "",
            address:"",
            averageEnergyConsumption:0,
            maximumEnergyConsumption:0,
            client_id:0})
        setIsFormOpen(true);
    }

    const onSubmitHandler = (device: DeviceModel) => {
        if (device.id === 0) {
            deviceService.insertDevice(device).then((response) => {
                device.id = response;
                setDevices([...devices, device]);
                setIsFormOpen(false);
            });
        }
        else {
            deviceService.updateDevice(device).then(() => {
                let index: number = devices.findIndex(q => q.id === device.id);
                devices[index] = device;
                setDevices(devices);
                setIsFormOpen(false);
            })
        }

    }

    const onCancelHandler = () => {
        setIsFormOpen(false);
    }

    const onEditHandler = (id: number) => {
        deviceService.getDeviceById(id).then((response) => {
            let device1=response;
            clientService.getClientById(response.client_id).then((response)=>{
                setCurrentClient(response);
                device1.client_id=response.id;
                setCurrentDevice(device1);
                setIsFormOpen(true);
            })

        })

    }

    return <>
        <Loader active={isLoading} size={"big"}>Loading</Loader>
        <DeviceList clients={clients} isLoading={isLoading} onDelete={onDeleteHandler} onCreate={onCreateHandler} devices={devices}
                    onEdit={onEditHandler}/>
        <DeviceForm isFormOpen={isFormOpen} onCancel={onCancelHandler} onSubmit={onSubmitHandler}
                    currentClient={currentClient!} currentDevice={currentDevice!} clients={clients}/>
    </>

}

export default Clients;