import React, {useEffect, useState} from "react";
import {Button, Form, Modal, Container, Select} from "semantic-ui-react";
import {ClientModel} from "../../Models/ClientModel";
import {DeviceModel} from "../../Models/DeviceModel";
import BlueButton from "../Styles/BlueButton";
import PurpleButton from "../Styles/PurpleButton";
import {Alert, Stack} from "@mui/material";



interface IDeviceForm {
    isFormOpen: boolean;
    onCancel: Function;
    onSubmit: Function;
    currentClient: ClientModel;
    currentDevice: DeviceModel;
    clients: ClientModel[];
}

interface IDropdownItem{
    key:number,
    value:number,
    text:string;
}

const DeviceForm: React.FunctionComponent<IDeviceForm> = (props) => {

    const [client, setClient] = useState<ClientModel>(props.currentClient);
    const [device, setDevice] = useState<DeviceModel>(props.currentDevice);
    const [error,setError] = useState<string>("");

    useEffect(()=>{
        setClient(props.currentClient);
        setDevice(props.currentDevice);
    },[props.currentClient,props.currentDevice])


    const onCancelHandler = () => {
        props.onCancel();
    }

    const onSubmitHandler = (d: DeviceModel) => {
        if(d.description.trim()==="")
        {
            setError("Invalid description");
            return;
        }
        if(d.address.trim()==="")
        {
            setError("Invalid address");
            return;
        }
        if(d.averageEnergyConsumption<0)
        {
            setError("Average energy consumption must be greater or equal to 0.");
            return;
        }
        if(d.maximumEnergyConsumption<0)
        {
            setError("Maximum energy consumption must be greater or equal to 0.");
            return;
        }
        if(d.client_id===0)
        {
            setError("The device must be associated to a client.");
            return;
        }
        props.onSubmit(d);
    }

    const onCloseModal = () => {
        setError("");
    }

    const mapToDropdownList = (cl:ClientModel):IDropdownItem =>{
        return {
            key:cl.id,
            value:cl.id,
            text:cl.firstName+" "+cl.lastName
        }
    }

    const onChangeDescriptionHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setDevice(prevState => ({
            ...prevState,
            description: event.target.value
        }))
    }


    const onChangeAddressHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setDevice(prevState => ({
            ...prevState,
            address: event.target.value
        }))
    }

    const onChangeMaximumHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setDevice(prevState => ({
            ...prevState,
            maximumEnergyConsumption:event.target.valueAsNumber
        }))
    }

    const onChangeAverageHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setDevice(prevState => ({
            ...prevState,
            averageEnergyConsumption:event.target.valueAsNumber
        }))
    }

    const onChangeClient = (event: React.ChangeEvent<HTMLSelectElement>, data:any) => {
        setClient(prevState => ({
            ...prevState,
            id: data.value
        }))
        setDevice(prevState => ({
            ...prevState,
            client_id: data.value
        }))
    }

    return <>
        <Modal
            size={"mini"}
            open={error!==""}
            onClose={onCloseModal}
        >
            <Alert severity="error">{error}</Alert>
        </Modal>
    <Modal
        open={props.isFormOpen}
        onClose={onCancelHandler}
        size={"mini"}
        dimmer={"blurring"}
    >
        <Modal.Header>
            <Container fluid textAlign="center">{device.id===0 ? "Create Device" : "Edit Device"}</Container>
        </Modal.Header>
        <Modal.Content>
            <Form>
                <Form.Field required>
                    <label>Description</label>
                    <input placeholder='Description' value={device.description} onChange={onChangeDescriptionHandler}/>
                </Form.Field>
                <Form.Field required>
                    <label>Address</label>
                    <input placeholder='Address' value={device.address} onChange={onChangeAddressHandler}/>
                </Form.Field>
                <Form.Field required>
                    <label>Maximum energy consumption</label>
                    <input placeholder='Maximum energy consumption' value={device.maximumEnergyConsumption} type={"number"} min={0} onChange={onChangeMaximumHandler}/>
                </Form.Field>
                <Form.Field required>
                    <label>Average energy consumption</label>
                    <input placeholder='Average energy consumption' type={"number"} min={0} value={device.averageEnergyConsumption} onChange={onChangeAverageHandler}/>
                </Form.Field>
                {device.id===0 &&
                <Form.Field
                    search
                    required
                    value={client.id}
                    onChange={onChangeClient}
                    label={"Client"}
                    control={Select}
                    options={props.clients.map(client => mapToDropdownList(client))}
                />}
                {device.id!==0 &&
                <Form.Field>
                    <label>Client</label>
                    <input placeholder='Client' value={client.firstName+" "+client.lastName} readOnly={true}/>
                </Form.Field>
                }
                <Form.Field>
                    <Stack direction="row"
                           justifyContent="center"
                           alignItems="center"
                           spacing={2}>
                        <BlueButton onClick={() => {
                            onSubmitHandler(device)
                        }} >{
                            device.id===0 ? "Create" : "Edit"}</BlueButton>
                        <PurpleButton onClick={onCancelHandler} type={"button"}>Close</PurpleButton>
                    </Stack>
                </Form.Field>
            </Form>
        </Modal.Content>

    </Modal>
        </>
}

export default DeviceForm;