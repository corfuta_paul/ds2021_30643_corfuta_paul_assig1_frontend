import React from "react";
import {Button, Table} from "semantic-ui-react";
import {ClientModel} from "../../Models/ClientModel";
import {DeviceModel} from "../../Models/DeviceModel";

interface IDeviceList {
    clients: ClientModel[];
    devices: DeviceModel[];
    isLoading: boolean;
    onCreate:Function;
    onEdit:Function;
    onDelete: Function;
}

const DeviceList:React.FunctionComponent<IDeviceList> = (props) =>{

    const onDeleteHandler = (id: number) =>{
        props.onDelete(id);
    }

    const onCreateHandler = () =>{
        props.onCreate();
    }

    const onEditHandler = (id: number) => {
        props.onEdit(id);
    }

    return <>
        { !props.isLoading && <Table striped >
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell width={2}><Button color={"teal"} onClick={onCreateHandler}>Create</Button></Table.HeaderCell>
                    <Table.HeaderCell width={3}>Description</Table.HeaderCell>
                    <Table.HeaderCell width={3}>Address</Table.HeaderCell>
                    <Table.HeaderCell>Maximum Energy Consumption</Table.HeaderCell>
                    <Table.HeaderCell>Average Energy Consumption</Table.HeaderCell>
                    <Table.HeaderCell>Client</Table.HeaderCell>
                </Table.Row>
            </Table.Header>

            <Table.Body>
                {props.devices.map(device =>(
                    <Table.Row key={device.id}>
                        <Table.Cell>
                            <Button icon="pencil" color="teal" onClick={()=>{onEditHandler(device.id)}}/>
                            <Button icon="trash" color="red" onClick={()=>{onDeleteHandler(device.id)}}/>
                        </Table.Cell>
                        <Table.Cell>{device.description}</Table.Cell>
                        <Table.Cell>{device.address}</Table.Cell>
                        <Table.Cell>{device.maximumEnergyConsumption}</Table.Cell>
                        <Table.Cell>{device.averageEnergyConsumption}</Table.Cell>
                        <Table.Cell>{props.clients.find(q=>q.id===device.client_id)?.firstName+ " " +props.clients.find(q=>q.id===device.client_id)?.lastName }</Table.Cell>
                    </Table.Row>
                ))}
            </Table.Body>
        </Table>}
    </>
}

export default DeviceList;