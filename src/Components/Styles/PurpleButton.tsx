import {styled} from "@mui/material/styles";
import Button from "@mui/material/Button";
import {deepPurple} from "@mui/material/colors";

const PurpleButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(deepPurple[200]),
    backgroundColor: deepPurple[200],
    '&:hover': {
        backgroundColor: deepPurple[400],
    },
}));

export default PurpleButton;