import Button from '@mui/material/Button';
import {styled} from "@mui/material/styles";
import {deepPurple, lightBlue} from "@mui/material/colors";


const BlueButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(lightBlue[200]),
    backgroundColor: lightBlue[200],
    '&:hover': {
        backgroundColor: lightBlue[400],
    },
}));



export default  BlueButton;
