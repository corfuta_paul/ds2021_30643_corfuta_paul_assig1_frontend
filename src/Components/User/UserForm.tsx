import React, {useEffect, useState} from "react";
import {Button, Form, Modal, Container, Select} from "semantic-ui-react";
import {UserModel} from "../../Models/UserModel";
import BlueButton from "../Styles/BlueButton";
import PurpleButton from "../Styles/PurpleButton";
import {Alert, Stack} from "@mui/material";


interface IUserForm {
    isFormOpen: boolean;
    onCancel: Function;
    onSubmit: Function;
    currentUser: UserModel;
}

const UserForm: React.FunctionComponent<IUserForm> = (props) => {

    const [user, setUser] = useState<UserModel>(props.currentUser);
    const [isValid,setIsValid] = useState<boolean>(false);
    const [password,setPassword] = useState<string>("");
    const [error, setError] = useState<string>("");

    useEffect(()=>{
        setUser(props.currentUser);
        setPassword(props.currentUser.password);
    },[props.currentUser])

    const onCancelHandler = () => {
        props.onCancel();
    }

    const onSubmitHandler = (u: UserModel) => {
        if(user.username.trim()===""){
            setError("Username invalid");
            return;
        }
        if(user.password.trim()===""){
            setError("Password invalid");
            return;
        }
        if(user.password!==password){
            setError("Passwords don't match");
            return;
        }
        props.onSubmit(u);
    }

    const onCloseModal = () => {
        setError("");
    }

    const onChangeUsernameHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setUser(prevState => ({
                ...prevState,
                username: event.target.value
            })
        )
    }

    const onChangePasswordHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setUser(prevState => ({
            ...prevState,
            password: event.target.value
        }))
    }

    const onConfirmHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setPassword(event.target.value);
    }

    const onChangeRoleHandler = (event: React.ChangeEvent<HTMLElement>, data: any) => {
        setUser(prevState => ({
            ...prevState,
            admin: data.value
        }))
    }

    return <>
        <Modal
            size={"mini"}
            open={error!==""}
            onClose={onCloseModal}
        >
            <Alert severity="error">{error}</Alert>
        </Modal>
    <Modal
        open={props.isFormOpen}
        onClose={onCancelHandler}
        size={"mini"}
        dimmer={"blurring"}
    >
        <Modal.Header>
            <Container fluid textAlign="center">{user.id===0 ? "Create User" : "Edit User"}</Container>
        </Modal.Header>
        <Modal.Content>
            <Form>
                <Form.Field required>
                    <label>Username</label>
                    <input placeholder='Username' value={user.username} onChange={onChangeUsernameHandler}/>
                </Form.Field>
                <Form.Field required>
                    <label>Password</label>
                    <input placeholder='Password' type={"password"} value={user.password} onChange={onChangePasswordHandler}/>
                </Form.Field>
                <Form.Field required>
                    <label>Confirm password</label>
                    <input placeholder='Confirm password' type={"password"} value={password} onChange={onConfirmHandler}/>
                </Form.Field>
                <Form.Field
                    required
                    value={user.admin}
                    label={"Role"}
                    control={Select}

                    onChange={onChangeRoleHandler}
                    options={[{key: "Admin", value: true, text: "Admin"}, {key: "User", value: false, text: "User"}]}
                />
                <Form.Field>
                    <Stack direction="row"
                           justifyContent="center"
                           alignItems="center"
                           spacing={2}>
                        <BlueButton onClick={() => {
                            onSubmitHandler(user)
                        }} >{
                            user.id===0 ? "Create" : "Edit"}</BlueButton>
                        <PurpleButton onClick={onCancelHandler} type={"button"}>Close</PurpleButton>
                    </Stack>
                </Form.Field>
            </Form>
        </Modal.Content>
    </Modal>
    </>
}

export default UserForm;