import React from "react";
import {UserModel} from "../../Models/UserModel";
import {Button, Table} from "semantic-ui-react";

interface IUserList {
    users: UserModel[];
    isLoading: boolean;
    onCreate:Function;
    onEdit:Function;
    onDelete: Function;
}

const UserList:React.FunctionComponent<IUserList> = (props) =>{

    const onDeleteHandler = (id: number) =>{
        props.onDelete(id);
    }

    const onCreateHandler = () =>{
        props.onCreate();
    }

    const onEditHandler = (id: number) => {
        props.onEdit(id);
    }

    return <>
        { !props.isLoading && <Table striped >
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell width={2}><Button color={"teal"} onClick={onCreateHandler}>Create</Button></Table.HeaderCell>
                    <Table.HeaderCell>Username</Table.HeaderCell>
                    <Table.HeaderCell>Role</Table.HeaderCell>
                </Table.Row>
            </Table.Header>

            <Table.Body>
                {props.users.map(user =>(
                    <Table.Row key={user.id}>
                        <Table.Cell>
                            <Button icon="pencil" color="teal" onClick={()=>{onEditHandler(user.id)}}/>
                            <Button icon="trash" color="red" onClick={()=>{onDeleteHandler(user.id)}}/>
                        </Table.Cell>
                        <Table.Cell>{user.username}</Table.Cell>
                        <Table.Cell>{!user.admin && "User"}{user.admin && "Admin"}</Table.Cell>
                    </Table.Row>
                ))}

            </Table.Body>
        </Table>}
    </>
}

export default UserList;