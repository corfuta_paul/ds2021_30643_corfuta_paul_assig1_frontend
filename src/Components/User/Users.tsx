import {Loader} from "semantic-ui-react";
import UserService from "../../Services/UserService";
import React, {useEffect, useState} from "react";
import {UserModel} from "../../Models/UserModel";
import UserForm from "./UserForm";
import UserList from "./UserList";
import AppMenu from "../AppMenu";

import {routes} from "../../App";

const userService: UserService = new UserService();

const Users: React.FunctionComponent<{}> = () => {
    const [users, setUsers] = useState<UserModel[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [error, setError] = useState<string>("");
    const [isFormOpen, setIsFormOpen] = useState<boolean>(false);
    const [currentUser, setCurrentUser] = useState<UserModel>({id: 0, username: "", password: "", admin: false});


    useEffect(() => {
        let isMounted = true;
        setIsLoading(true);

        let fn = async () => {
            try {
                let users = await userService.getUsers();
                if (isMounted) {
                    setUsers(users);
                    setIsLoading(false);
                }
            } catch (error) {
                console.log(error);
                if (isMounted) {
                    setError("Something went wrong while getting users/clients");
                }
            }
        }

        fn();

        return () => {
            isMounted = false;
        }
    }, [])

    const onDeleteHandler = (id: number) => {
        console.log(id);
        userService.deleteUser(id).then(() => {
            setUsers(users.filter(q => q.id !== id));
        });

    }

    const onCreateHandler = () => {
        setCurrentUser({id: 0, username: "", password: "", admin: false});
        setIsFormOpen(true);
    }

    const onSubmitHandler = (user: UserModel) => {
        if (user.id === 0) {
            userService.insertUser(user).then((response) => {
                user.id = response;
                setUsers([...users, user]);
                setIsFormOpen(false);
            });
        } else {
            userService.updateUser(user).then(() => {
                let index: number = users.findIndex(q => q.id === user.id);
                users[index] = user;
                setUsers(users);
                setIsFormOpen(false);
            })
        }
    }

    const onCancelHandler = () => {
        setIsFormOpen(false);
    }

    const onEditHandler = (id: number) => {
        userService.getUserById(id).then((response) => {
            setCurrentUser(response);
            setIsFormOpen(true);
        })


    }

    return <>
        <Loader active={isLoading} size={"big"}>Loading</Loader>
        <UserList users={users} isLoading={isLoading} onDelete={onDeleteHandler} onCreate={onCreateHandler}
                  onEdit={onEditHandler}/>
        <UserForm isFormOpen={isFormOpen} onCancel={onCancelHandler} onSubmit={onSubmitHandler}
                  currentUser={currentUser!}/>
    </>
}

export default Users;