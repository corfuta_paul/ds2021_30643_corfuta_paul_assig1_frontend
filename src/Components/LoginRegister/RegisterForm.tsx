import React, {useEffect, useState} from "react";
import {Button, Card, CardContent, CardHeader, Container, Form} from "semantic-ui-react";
import UserService from "../../Services/UserService";
import ClientService from "../../Services/ClientService";
import "./center.css";
import background from "../../Images/background-website-light-blue-wallpapers-background-2.jpg";
import {ClientModel} from "../../Models/ClientModel";
import {UserModel} from "../../Models/UserModel";
import AppMenu from "../AppMenu";
import {routes} from "../../App";
import {useHistory} from "react-router-dom";

const userService:UserService= new UserService();
const clientService:ClientService = new ClientService();

const RegisterForm: React.FunctionComponent<{}> = () => {

    const[client,setClient]=useState<ClientModel>({id: 0, firstName: "", lastName: "", address: "", birthDate: new Date(), user_id: 0});
    const[user,setUser]=useState<UserModel>({id: 0, username: "", password: "", admin: false});
    const[confirmPassword,setConfirmPassword]=useState<string>("");
    const[isValid,setIsValid]=useState<boolean>(false);

    const history = useHistory();

    useEffect(()=>{
        if( client.firstName.trim()!=="" &&
            client.lastName.trim()!=="" &&
            client.address.trim()!=="" &&
            new Date(client.birthDate).getFullYear()<2004 &&
            user.username.trim()!=="" &&
            user.password.trim()!=="" &&
            confirmPassword===user.password){
            setIsValid(true);
            return;
        }
        setIsValid(false);
    },[client,user,confirmPassword])

    const onChangeLastNameHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setClient(prevState => ({
            ...prevState,
            lastName: event.target.value
        }))
    }

    const onChangeFirstNameHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setClient(prevState => ({
            ...prevState,
            firstName: event.target.value
        }))
    }


    const onChangeAddressHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setClient(prevState => ({
            ...prevState,
            address: event.target.value
        }))
    }

    const onChangeBirthDateHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setClient(prevState => ({
            ...prevState,
            birthDate: new Date(event.target.value)
        }))
        console.log(new Date(event.target.value));
    }

    const onChangeUsernameHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setUser(prevState => ({
                ...prevState,
                username: event.target.value
            })
        )
    }

    const onChangePasswordHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setUser(prevState => ({
            ...prevState,
            password: event.target.value
        }))
    }

    const onChangeConfirmPasswordHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setConfirmPassword(event.target.value);
    }

    const onRegisterHandler = () =>{
        userService.insertUser(user).then((response)=>{
            client.user_id=response;
            clientService.insertClient(client).then(()=>{
                history.push("/login");
            })
        })
    }

    return<>
    <Card centered color={"teal"} className={"vertical-center"} >
        <CardHeader textAlign={"center"}><h3>Register</h3></CardHeader>
        <CardContent>
            <Form size={"tiny"}>
                <Form.Field required>
                    <label>First Name</label>
                    <input placeholder='First Name' onChange={onChangeFirstNameHandler}/>
                </Form.Field>
                <Form.Field required>
                    <label>Last Name</label>
                    <input placeholder='Last Name' onChange={onChangeLastNameHandler}/>
                </Form.Field>
                <Form.Field required>
                    <label>Birth date</label>
                    <input placeholder='Birth date' type={"date"} onChange={onChangeBirthDateHandler}/>
                </Form.Field>
                <Form.Field required>
                    <label>Address</label>
                    <input placeholder='Address' onChange={onChangeAddressHandler}/>
                </Form.Field>
                <Form.Field required>
                    <label>Username</label>
                    <input placeholder='Username' onChange={onChangeUsernameHandler}/>
                </Form.Field>
                <Form.Field required>
                    <label>Password</label>
                    <input placeholder='Password' type={"password"} onChange={onChangePasswordHandler}/>
                </Form.Field>
                <Form.Field required>
                    <label>Confirm password</label>
                    <input placeholder='Confirm password' type={"password"} onChange={onChangeConfirmPasswordHandler}/>
                </Form.Field>

                <Container textAlign={"center"}>
                <Button type='submit' color={"teal"} onClick={onRegisterHandler} disabled={!isValid}>Register</Button>
                </Container>
            </Form>
        </CardContent>
    </Card>
        </>

}
export default RegisterForm;