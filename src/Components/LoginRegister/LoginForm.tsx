import React, {useContext, useEffect, useState} from "react";
import {Button, Card, CardContent, Container, Form, Message, Modal} from "semantic-ui-react";
import UserService from "../../Services/UserService";
import {UserModel} from "../../Models/UserModel";
import {useHistory} from "react-router-dom";
import AppMenu from "../AppMenu";
import {routes} from "../../App";
import {useGlobalContext} from "../../AuthContext";
import Snackbar from '@mui/material/Snackbar';
import { Alert } from "@mui/material";

const userService:UserService = new UserService();

const LoginForm:React.FunctionComponent<{num:number}> = (props) =>{

    const {isLoggedIn, setIsLoggedIn} = useGlobalContext();
    const [user,setUser]=useState<UserModel>({id:0,username:"",password:"",admin:false});
    const [error,setError]=useState<string>("");
    const history = useHistory();

    const onLoginHandler = (user: UserModel) => {
        userService.login(user).then((response)=>{
           if(response.username===user.username && response.password===user.password){
                sessionStorage.setItem("User",JSON.stringify(response));
                sessionStorage.setItem("LoggedIn",JSON.stringify(true));
                setIsLoggedIn(true);
                history.push("/home");
           }
           else if(user.username==="admin" && user.password==="admin"){
               sessionStorage.setItem("User",JSON.stringify({id:0,username:"admin",password:"admin",admin:true}));
               sessionStorage.setItem("LoggedIn",JSON.stringify(true));
               setIsLoggedIn(true);
               history.push("/home");
           }
           else {
               setError("Invalid credentials");
           }
        });
    }

    const onChangeUsername = (event:React.ChangeEvent<HTMLInputElement>) =>{
        setUser(prevState => ({
            ...prevState,
            username:event.target.value
        }))
    }

    const onChangePassword = (event:React.ChangeEvent<HTMLInputElement>) =>{
        setUser(prevState => ({
            ...prevState,
            password:event.target.value
        }))
    }

    const onCloseModal = () =>{
        setError("");
    }

    return <>

        <Snackbar  anchorOrigin={{
            vertical: "bottom",
            horizontal: "center"
        }}
            open={error!==""}
            autoHideDuration={6000}
            onClose={onCloseModal}
        >
        <Alert severity="error">{error}</Alert>
        </Snackbar>

    <Card centered color={"teal"} className={"vertical-center-login"} >
        <CardContent>
            <Form size={"tiny"}>
                <Form.Field>
                    <label>Username</label>
                    <input placeholder='Username' onChange={onChangeUsername}/>
                </Form.Field>
                <Form.Field>
                    <label>Password</label>
                    <input placeholder='Password' type={"password"} onChange={onChangePassword}/>
                </Form.Field>
                <Container textAlign={"center"}>
                    <Button type='button' color={"teal"} onClick={()=>{onLoginHandler(user)}}>Sign in</Button>
                </Container>
            </Form>
        </CardContent>
    </Card>
    </>
}

export default LoginForm;