import {Container, Loader} from "semantic-ui-react";
import AppMenu from "../AppMenu";
import {routes} from "../../App";
import React, {useEffect, useState} from "react";
import {UserModel} from "../../Models/UserModel";
import ClientService from "../../Services/ClientService";
import {ClientModel} from "../../Models/ClientModel";

const clientService:ClientService = new ClientService();

const Home:React.FunctionComponent<{}> = () => {

    const [name,setName]=useState<string>("");
    const [clients,setclients]= useState<ClientModel[]>([]);
    const [isLoading,setIsLoading] = useState<boolean>(true);
    const [currentClient, setCurrentClient] = useState<ClientModel>({
        id: 0,
        firstName: "",
        lastName: "",
        address: "",
        birthDate: new Date(),
        user_id: 0
    });

    useEffect(()=>{
        setIsLoading(true);
        clientService.getClients().then((response)=>{
            setclients(response);
        });

    },[])
    useEffect(()=>{
        const user:UserModel = JSON.parse(sessionStorage.getItem("User")!);
        if (user!=null){
            const client:ClientModel = clients.find(q=>q.user_id===user.id)!;
            setCurrentClient(client);
        }
        setIsLoading(false);
    },[clients])

    return <Container fluid>
        <Loader active={isLoading}>
            Loading...
        </Loader>
        {!isLoading && <h1>{currentClient?.id!==0 ? `WELCOME, ${currentClient?.firstName} ${currentClient?.lastName}` : "WELCOME"}</h1>}
    </Container>
}

export default Home;