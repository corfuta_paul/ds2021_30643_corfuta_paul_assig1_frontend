import React, {useEffect, useState} from "react";
import {Button, Card, CardActions, CardContent, Stack, Typography} from "@mui/material";
import {ClientModel} from "../../Models/ClientModel";
import {DeviceModel} from "../../Models/DeviceModel";
import ClientService from "../../Services/ClientService";
import DeviceService from "../../Services/DeviceService";
import {useGlobalContext} from "../../AuthContext";
import {UserModel} from "../../Models/UserModel";
import BlueButton from "../Styles/BlueButton";
import ClientFormUser from "./ClientFormUser";
import {deepPurple, indigo} from "@mui/material/colors";
import {Loader} from "semantic-ui-react";
import DeviceList from "./DeviceListUser";
import PurpleButton from "../Styles/PurpleButton";
import SensorService from "../../Services/SensorService";
import MonitoredValueService from "../../Services/MonitoredValueService";
import {SensorModel} from "../../Models/SensorModel";
import {MonitoredValueModel} from "../../Models/MonitoredValueModel";

const clientService: ClientService= new ClientService();
const deviceService: DeviceService= new DeviceService();
const sensorService: SensorService= new SensorService();
const monitoredValueService: MonitoredValueService = new MonitoredValueService();

const Profile: React.FunctionComponent<{}> = () =>{

    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [isFormOpen, setIsFormOpen] = useState<boolean>(false);
    const [currentClient, setCurrentClient] = useState<ClientModel>({id: 0, firstName: "", lastName: "", address: "", birthDate: new Date(), user_id: 0});
    const [currentDevices, setCurrentDevices] = useState<DeviceModel[]>([]);
    const [currentDevice, setCurrentDevice] = useState<DeviceModel>({  id: 0, description: "", address:"", averageEnergyConsumption:0, maximumEnergyConsumption:0, client_id:0});
    const [currentSensor, setCurrentSensor] = useState<SensorModel>({id: 0,sensorDescription:"", maximumValue:0, device_id:0});
    const [currentMonitoredValues, setCurrentMonitoredValues] = useState<MonitoredValueModel[]>([]);
    const {isLoggedIn, setIsLoggedIn} = useGlobalContext();
    const [error, setError] = useState<string>("");
    const [average,setAverage] = useState<number>(0);

    useEffect(()=>{
        let isMounted = true;
        setIsLoading(true);

        let fn = async () => {
            try {
                const user:UserModel=JSON.parse(sessionStorage.getItem("User")!);
                let client = await clientService.getClientByUserId(user.id);
                let devices = await deviceService.getDevicesByClientId(client.id);


                if (isMounted) {
                    setCurrentClient(client)
                    setCurrentDevices(devices);
                    setIsLoading(false);
                }
            } catch (error) {
                console.log(error);
                if (isMounted) {
                    setError("Something went wrong while getting users/clients");
                }
            }
        }

        fn();

        return () => {
            isMounted = false;
        }

    },[isLoggedIn])

    const onEdit = (c:ClientModel) => {
        clientService.updateClient(c).then(()=>{
            setCurrentClient(c);
            setIsFormOpen(false);
        })
    }

    const onDeleteDevice = (id:number) => {
        deviceService.deleteDevice(id);
        setCurrentDevices(currentDevices.filter(q=>q.id!==id));
    }

    const openForm = () => {
        setIsFormOpen(true);
    }

    const  onCancel = () => {
        setIsFormOpen(false);
    }

    const onRefreshData = (id: number) => {
       deviceService.getDeviceById(id).then((response)=>{
           setCurrentDevice(response);
           sensorService.getSensorByDeviceId(response.id).then((response)=>{
               setCurrentSensor(response);
               monitoredValueService.getMonitoredValuesBySensorId(response.id).then((response)=>{
                   setCurrentMonitoredValues(response);
               })
           })
       })
    }

    useEffect(()=>{
        let maximum: number = 0 ;
        let average: number = 0;
        if(currentMonitoredValues.length>0){
            currentMonitoredValues.forEach(mv=>{
                average = average + mv.energyConsumption;
                if(mv.energyConsumption>maximum){
                    maximum = mv.energyConsumption;
                }
            })
            average= average/currentMonitoredValues.length;
            setAverage(average);
            setCurrentSensor(prevState => ({
                ...prevState,
                maximumValue : maximum
            }))
            setCurrentDevice(prevState => ({
                ...prevState,
                maximumEnergyConsumption: maximum
            }))
            setCurrentDevice(prevState => ({
                ...prevState,
                averageEnergyConsumption: average
            }))
        }
    },[currentMonitoredValues])

    useEffect(()=>{
        if(average!==0) {
            sensorService.updateSensor(currentSensor).then(() => {
                deviceService.updateDevice(currentDevice).then(() => {
                    let index: number = currentDevices.findIndex(q => q.id === currentDevice.id)
                    currentDevices.splice(index,1,currentDevice)
                    setCurrentDevices(currentDevices);
                })
            });
        }
    },[average])

    return <div>
        <Loader active={isLoading} size={"big"}>Loading</Loader>
    {!isLoading && <Stack alignItems={"center"} ><h2>Client</h2>
        <Card sx={{ minWidth: 275 }} style={{backgroundColor: indigo[50]}}>
            <CardContent>
                <Typography sx={{ fontSize: 18}} align={"center"} gutterBottom>
                    <b> {currentClient.firstName + " " + currentClient.lastName} </b>
                </Typography>
                <Typography >
                    <b>Address: </b>{currentClient.address}
                </Typography>
                <Typography>
                    <b>Birth Date: </b>{new Date(currentClient.birthDate).toLocaleDateString("ro-RO")}
                </Typography>
                <Stack alignItems={"center"} paddingTop={2}>
                    <BlueButton size="small" onClick={openForm}>Edit Profile</BlueButton>
                </Stack>
            </CardContent>
        </Card>
        <ClientFormUser currentClient={currentClient} onSubmit={onEdit} isFormOpen={isFormOpen} onCancel={onCancel}/>
        <h2>Devices</h2>
        <DeviceList devices={currentDevices} onDelete={onDeleteDevice} onRefresh={onRefreshData}/>
    </Stack>}
    </div>
}

export default Profile;