import React, {useEffect, useState} from "react";
import { Form, Modal, Container, Select} from "semantic-ui-react";
import {ClientModel} from "../../Models/ClientModel";
import {UserModel} from "../../Models/UserModel";
import BlueButton from "../Styles/BlueButton";
import PurpleButton from "../Styles/PurpleButton";
import {Alert, Stack} from "@mui/material";


interface IClientForm {
    isFormOpen: boolean;
    onCancel: Function;
    onSubmit: Function;
    currentClient: ClientModel;
}

const ClientFormUser: React.FunctionComponent<IClientForm> = (props) => {

    const [client, setClient] = useState<ClientModel>(props.currentClient);
    const [error, setError] = useState<string>("");

    useEffect(()=>{
        setClient(props.currentClient);
    },[props.currentClient])

    const onCancelHandler = () => {
        props.onCancel();
    }

    const onSubmitHandler = (c: ClientModel) => {
        if(client.firstName.trim()===""){
            setError("The first name is not valid");
            return;
        }
        if(client.lastName.trim()===""){
            setError("The last name is not valid");
            return;
        }
        if(client.address.trim()===""){
            setError("The address is not valid");
            return;
        }
        if( new Date().getFullYear() - new Date(client.birthDate).getFullYear()<18){
            setError("You must be 18 years old");
            return;
        }
        if(client.user_id===0){
            setError("The client must be associated to a user");
            return;
        }
        props.onSubmit(c);

    }

    const onCloseModal = () => {
        setError("");
    }

    const onChangeFirstNameHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setClient(prevState => ({
                ...prevState,
                firstName: event.target.value
            })
        )
    }

    const onChangeLastNameHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setClient(prevState => ({
            ...prevState,
            lastName: event.target.value
        }))
    }

    const onChangeAddressHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setClient(prevState => ({
            ...prevState,
            address: event.target.value
        }))
    }

    const onChangeBirthDateHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setClient(prevState => ({
            ...prevState,
            birthDate: new Date(event.target.value)
        }))
    }


    return <>
        <Modal
            size={"mini"}
            open={error!==""}
            onClose={onCloseModal}
        >
            <Alert severity="error">{error}</Alert>
        </Modal>
        <Modal
            open={props.isFormOpen}
            onClose={onCancelHandler}
            size={"mini"}
            dimmer={"blurring"}
        >
            <Modal.Header>
                <Container fluid textAlign="center">{client.id===0 ? "Create Client" : "Edit Client"}</Container>
            </Modal.Header>
            <Modal.Content>
                <Form>
                    <Form.Field required>
                        <label>First Name</label>
                        <input placeholder='First Name' value={client.firstName} onChange={onChangeFirstNameHandler}/>
                    </Form.Field>
                    <Form.Field required>
                        <label>Last Name</label>
                        <input placeholder='Last Name' value={client.lastName} onChange={onChangeLastNameHandler}/>
                    </Form.Field>
                    <Form.Field required>
                        <label>Address</label>
                        <input placeholder='Address' value={client.address} onChange={onChangeAddressHandler}/>
                    </Form.Field>
                    <Form.Field required>
                        <label>Birth Date</label>
                        <input placeholder='Birth Date' type={"date"} min='1899-01-01' max='2003-10-30' value={`${new Date(client.birthDate).toLocaleDateString("fr-CA")}`} onChange={onChangeBirthDateHandler}/>
                    </Form.Field>
                    <Form.Field>
                        <Stack direction="row"
                               justifyContent="center"
                               alignItems="center"
                               spacing={2}>
                            <BlueButton onClick={() => {
                                onSubmitHandler(client)
                            }} >Edit</BlueButton>
                            <PurpleButton onClick={onCancelHandler} type={"button"}>Close</PurpleButton>
                        </Stack>
                    </Form.Field>
                </Form>
            </Modal.Content>

        </Modal>
    </>
}

export default ClientFormUser;