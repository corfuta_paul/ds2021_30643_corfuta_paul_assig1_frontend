import React, {useState} from 'react';
import './App.css';
import Users from './Components/User/Users';
import AppMenu from "./Components/AppMenu";
import Home from './Components/Home/Home';
import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom';
import Clients from "./Components/Client/Clients";
import Devices from "./Components/Device/Devices";
import Sensors from "./Components/Sensor/Sensors";
import MonitoredValues from "./Components/MonitoredValue/MonitoredValues";
import RegisterForm from "./Components/LoginRegister/RegisterForm";
import LoginForm from "./Components/LoginRegister/LoginForm";
import Profile from "./Components/UserProfile/Profile";
import { MyGlobalContext } from "./AuthContext";
import {PrivateRoute} from "./PrivateRoute";
import {makeStyles} from "@material-ui/core/styles"

const useStyles =  makeStyles((theme)=>({
    root: {
        minHeight: "100vh",
        backgroundImage: `url(https://images7.alphacoders.com/996/thumb-1920-996873.jpg)`
    }
}))

export let routes = [
    {name: "Users", path: "/users", component: Users},
    {name: "Clients", path: "/clients", component: Clients},
    {name: "Devices", path: "/devices", component: Devices},
    {name: "Sensors", path: "/sensors", component: Sensors},
    {name: "Monitored Values", path: "/monitoredvalues", component: MonitoredValues}
]

function App() {

    const [isLoggedIn,setIsLoggedIn] = useState<boolean>(false);
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <MyGlobalContext.Provider value= {{ isLoggedIn, setIsLoggedIn }}>
            <BrowserRouter>
                <AppMenu routes={routes.map(route => ({name: route.name, path: route.path}))}/>
                <Switch>
                    <Redirect exact from="/" to="/home"/>
                    <Route exact path="/home" component={Home} />
                    {
                        routes.map((route, index) => (
                            <PrivateRoute key={index} exact path={route.path} component={route.component} isAuthenticated={isLoggedIn}/>
                        ))
                    }
                    <Route exact path="/register" component={RegisterForm} />
                    <Route exact path="/login" component={LoginForm} />
                    <PrivateRoute exact path="/profile" component={Profile} isAuthenticated={isLoggedIn}/>
                </Switch>
            </BrowserRouter>
            </MyGlobalContext.Provider>
        </div>

    );
}

export default App;
